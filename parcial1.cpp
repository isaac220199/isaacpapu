#include <iostream>
#include <conio.h>

using namespace std;
void tipo_monedas();
void precios();
void analizar();

float moneda_local=0,moneda_extranjera=0,precio_local=0,precio_extranjero=0;
float local=0,extranjero=0,impuesto_local=0,impuesto_extranjero=0;
int band=0;

int main()
{
    tipo_monedas();
    precios();
    analizar();

    getch();
    return 0;
}
void tipo_monedas()
{
    volver:
    cout<<"Que tipo de moneda ocupas en tu pais?"<<endl;
    cout<<"1.-REAL(Brasilero)"<<endl;
    cout<<"2.-EURO(Europeo)"<<endl;
    cout<<"3.-DOLAR(Americano)"<<endl;
    cout<<"4.-Boliviano(Bolivia)"<<endl;
    cin>>band;

    switch(band)
    {
        case 1:
        moneda_local = 0.3207;
        impuesto_local = 0.18;
        break;
        case 2:
        moneda_local = 1.19534;
        impuesto_local =0.06;
        break;
        case 3:
        moneda_local = 1;
        impuesto_local =0.088;
        break;
        case 4:
        moneda_local = 0.144614;
        impuesto_local =0.1495;
        break;
        default:
        cout<<"No ingreso una opcion valida"<<endl;
        goto volver;
    }
    volver1:
    cout<<"Que tipo de moneda ocupan en el pais que te encuentras?"<<endl;
    cout<<"1.-REAL(Brasilero)"<<endl;
    cout<<"2.-EURO(Europeo)"<<endl;
    cout<<"3.-DOLAR(Americano)"<<endl;
    cout<<"4.-Boliviano(Bolivia)"<<endl;
    cin>>band;

    switch(band)
    {
        case 1:
        moneda_extranjera = 0.3207;
        impuesto_extranjero = 0.43-0.18;
        break;
        case 2:
        moneda_extranjera = 1.19534;
        impuesto_extranjero =0.43-0.06;
        break;
        case 3:
        moneda_extranjera = 1;
        impuesto_extranjero =0.43-0.088;
        break;
        case 4:
        moneda_extranjera = 0.144614;
        impuesto_extranjero =0.43-0.1495;
        break;
        default:
        cout<<"No ingreso una opcion valida"<<endl;
        goto volver1;
    }
}

void precios()
{
    cout<<"Cuanto te cuesta el producto en tu pais con tu moneda local?"<<endl;
    cin>>precio_local;
    cout<<"Cuanto cuesta el producto en el extranjero con la moneda extranjera?"<<endl;
    cin>>precio_extranjero;
}

void analizar()
{
    local = moneda_local*precio_local;
    local = local+(local*impuesto_local);
    extranjero = moneda_extranjera*precio_extranjero;
    extranjero = extranjero-(extranjero*impuesto_extranjero);
    if(local>extranjero)
    {
        cout<<"Te conviene comprar el producto en el extranjero"<<endl;
    }
    else if(local<extranjero)
    {
        cout<<"Te conviene comprarte el produto en tu pais"<<endl;
    }
    else
    {
        cout<<"No hay diferencia en ccomprarlo en el extranjero o en tu pais local"<<endl;
    }
    cout<<"Precio local en dolares: "<<local<<endl;
    cout<<"Precio extranjero en dolares: "<<extranjero<<endl;
}
